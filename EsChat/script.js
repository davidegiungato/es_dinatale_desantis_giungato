let utente = [];
let nickname = "";

$( "#btn_enter" ).click(function() {
    nickname = document.getElementById("insert_nick").value;

    if(nickname != ""){
        window.location = "chat.html";
    }
    else
    {
        alert("Inserire il nickname prima di entrare!");
    }
});

$( "#btn_send" ).click(function() {
    insMsg();
});

function insMsg(){
    let var_msg = document.getElementById("insert_msg").value;


    let messaggio = {
        id: calcolaId(),
        nick: "Nick-N.D",
        msg: var_msg
    };

    utente.push(messaggio);

    showMsg();
}

// ID INCREMENTALE
function calcolaId(){
    if(utente.length != 0)
        return utente[utente.length - 1].id + 1;

    return 1;
}

// MOSTRA MESSAGGIO
function showMsg(){
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
    let html_section = "";
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
    for(let [index, msg] of utente.entries())
    {
        html_section += `
            <tr>
                <td>
                    <h1>${msg.nick} - ${msg.data}</h1>
                    <p>${msg.msg}</p>
                </td>
            </tr>
        `;
    }
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
    document.getElementById("tab_chat").innerHTML = html_section;
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
}